//Deficion parametros Aplicacion
var express = require('express');         //Referencia Biblioteca
var bodyParser = require('body-parser');
var requestJSON = require('request-json'); //Biblioteca para llamados JSON
var app = express();                      //Creaciòn App
var port = process.env.PORT || 3000;      //Puerto de escucha
var usersFile = require('./users.json');  //Archivo insumo
var URLbase = "/colapi/v3/";              //URL de inicio

var baseMLabURL = 'https://api.mlab.com/api/1/databases/mlrdgm/collections/';
var apiKeyMLab = 'apiKey=R20lypodSxRR7EwDEp_yZSpXV0niPGoO';

app.listen(port);
app.use(bodyParser.json());
console.log("Colapi escuchando en puerto " + port + "..." );

// GET by UserId
app.get(URLbase + 'users/:id',
 function (req, res) {
   console.log("GET /colapi/v3/users/:id");
   console.log(req.params.id);
   var id = req.params.id;
   var queryString = 'q={"id":"'+ id +'"}&f={"_id":0}&';
   var httpClient = requestJSON.createClient(baseMLabURL);
   httpClient.get('users?' + queryString + apiKeyMLab,
     function(errMLab, respuestaMLab, bodymLab) {
     console.log('Error : ' + errMLab);
     var respuestaBody = {};
     respuestaBody = !errMLab ? bodymLab : {"msg" : "Error al recuperar users de mLab"};
     console.log("bodymLab"+respuestaBody);
     res.send(respuestaBody[0])  ;
   });
});


//LOGIN mLab
app.post(URLbase + 'loginmlab',
  function(req, res) {
    console.log("LOGINmLab/colapi/V3");
    var password = req.body.password;
    var email = req.body.email;
    console.log(req.body);
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    var queryString1 = 'q={"email":"';
    var queryString2 ='"}&';
    // Se consulta usuario por email y password
    clienteMlab.get('users?' + queryString1 + email + queryString2 + apiKeyMLab,
      function(errMLab, respuestaMLab, bodymLab) {
      var consulta = {};
      consulta = bodymLab[0];
      if(consulta != null){
        console.log("Se esta consultando el usuario: ["+consulta.id+"]");
        var passwordBD = consulta.password;
        if(password == passwordBD) {
          consulta.logged = true;
          var cambio = '{"$set":' + JSON.stringify(consulta) + '}';
          queryString1 = 'q={"id":"';
          // Se actualiza el estado del usuario a traves de su id
          clienteMlab.put('users?'+ queryString1 + consulta.id + queryString2 + apiKeyMLab, JSON.parse(cambio),
            function(errMLab2, respuestaMLab2, bodymLab2) {
            console.log("El usuario: ["+consulta.id+"] se ha logeado correctamente");
            res.send({"id" : consulta.id, "email" : email, "msg" : "Login correcto."},200);
          });
         } else {
           console.log("El usuario: ["+consulta.id+"] ha ingresado un password incorrecto");
           res.send({"email" : email, "msg" : "Datos ingresados incorrectos."},404);
         }
      }else{
        console.log("El correo: ["+email+"] no existe en la BD");
        res.send({"email" : email, "msg" : "Correo no existe."},404);
      }
    });
  });


//LOGOUT mLab
  app.post(URLbase + 'logoutmlab',
    function(req, res) {
      console.log("LOGOUTmLab/colapi/V3");
      var password = req.body.password;
      var email = req.body.email;
      var clienteMlab = requestJSON.createClient(baseMLabURL);
      var queryString1 = 'q={"email":"';
      var queryString2 ='"}&';
      // Se consulta usuario por email y password
      clienteMlab.get('users?' + queryString1 + email + queryString2 + apiKeyMLab,
        function(errMLab, respuestaMLab, bodymLab) {
        var consulta = {};
        consulta = bodymLab[0];
        if(consulta != null){
          console.log("Se esta consultando el usuario: ["+consulta.id+"]");
          var passwordBD = consulta.password;
          if(password == passwordBD) {
            var nuevoEstado = {"logged": true};
            var cambio = '{"$unset":' + JSON.stringify(nuevoEstado) + '}';
            queryString1 = 'q={"id":"';
            // Se actualiza el estado del usuario a traves de su id
            clienteMlab.put('users?'+ queryString1 + consulta.id + queryString2 + apiKeyMLab, JSON.parse(cambio),
              function(errMLab2, respuestaMLab2, bodymLab2) {
              console.log("El usuario: ["+consulta.id+"] se ha deslogeado correctamente");
              res.send({"email" : email, "msg" : "Logout correcto."},200);
            });
           } else {
             console.log("El usuario: ["+consulta.id+"] ha ingresado un password incorrecto");
             res.send({"email" : email, "msg" : "Datos ingresados incorrectos."},404);
           }
        }else{
          console.log("El correo: ["+email+"] no existe en la BD");
          res.send({"email" : email, "msg" : "Correo no existe."},404);
        }
      });
    });
