//Deficion parametros Aplicacion
var express = require('express');         //Referencia Biblioteca
var bodyParser = require('body-parser');
var app = express();                      //Creaciòn App
var port = process.env.PORT || 3000;      //Puerto de escucha
var usersFile = require('./users.json');  //Archivo insumo
var URLbase = "/colapi/v2/";              //URL de inicio

app.listen(port, function(){
 console.log("API apicol escuchando en el puerto " + port + "...");
});

app.use(bodyParser.json());


// GET users
app.get(URLbase + 'users',
 function(request, response) {
   console.log(URLbase);
   console.log(usersFile);
   console.log("GET all users.");
   response.send(usersFile);
});

// Petición GET con parámetros (req.params)
app.get(URLbase + 'users/:id',
 function (req, res) {
   if(usersFile.length < req.params.id || req.params.id == "0"){
     res.send({"msg" : "Invalid id["+req.params.id+"]"});
   }
   for(let i = 0;i < usersFile.length; i++) {
     if(usersFile[i].id == req.params.id) {
       res.send(usersFile[i]);
     }
   }
});

// Petición GET con Query String (req.query)
app.get(URLbase + 'users',
 function(req, res) {
   console.log("GET con query string.");
   console.log(req.query.id);
   console.log(req.query.country);
   res.send(usersFile[pos - 1]);
   respuesta.send({"msg" : "GET con query string"});
});

// Petición POST (reg.body)
app.post(URLbase + 'users',
 function(req, res) {
   var newID = usersFile.length + 1;
   var newUser = {
     "id" : newID,
     "first_name" : req.body.first_name,
     "last_name" : req.body.last_name,
     "email" : req.body.email,
     "password" : req.body.password
   };
   usersFile.push(newUser);
   console.log(usersFile);
   writeUserDataToFile(usersFile);
   res.send({"msg" : "Usuario creado correctamente: ", newUser});
 });

 // Petición PUT
app.put(URLbase + 'users/:id',
  function(req, res){
    console.log("PUT /colapi/v2/users/:id");
    var idBuscar = req.params.id;
    var encontrado = false;
    for(i = 0; (i < usersFile.length) && !encontrado; i++) {
      if(usersFile[i].id == idBuscar) {
        var updateUser = {
          "id" : usersFile[i].id,
          "first_name" : req.body.first_name,
          "last_name" : req.body.last_name,
          "email" : req.body.email,
          "password" : req.body.password
        };
        usersFile[i] = updateUser;
        encontrado = true;
        writeUserDataToFile(usersFile);
        res.send({"msg" : "Usuario actualizado correctamente.", updateUser});
      }
    }
    if(!encontrado) {
      res.send({"msg" : "Usuario no encontrado.", updateUser});
    }
  });

// Petición DELETE
app.delete(URLbase + 'users/:id',
 function(req, res) {
   const id = req.params.id-1;
   const reg = usersFile[id];
   if(undefined != reg){
     usersFile.splice(id,1);
     writeUserDataToFile(usersFile);
     res.send(204);
  } else
    res.send(404);
});

// Petición POST LOGIN
app.post(URLbase + 'login',
  function(req, res) {
  console.log ("POST LOGIN");
  console.log(req.body.email);
  var email = req.body.email;
  var password = req.body.password;
  for(usuario of usersFile) {
    if (usuario.email == email && usuario.password == password){
      usuario.logged = true;
      writeUserDataToFile(usersFile);
      console.log("Login correcto");
      res.send({"msg" : "Login correcto.", "idUsuario" : req.body.email});
    }
   }
   res.send({"msg" : "Credenciales incorrectas.", "idUsuario" : req.body.email});
});

// Petición POST LOGOUT
app.post(URLbase + 'logout',
  function(req, res) {
  console.log ("POST LOGOUT");
  console.log(req.body.email);
  var email = req.body.email;
  var password = req.body.password;
  for(usuario of usersFile) {
    if (usuario.email == email && usuario.password == password){
      delete usuario.logged;
      writeUserDataToFile(usersFile);
      console.log("Logout correcto");
      res.send({"msg" : "Logout correcto.", "idUsuario" : req.body.email});
    }
   }
   res.send({"msg" : "Credenciales incorrectas.", "idUsuario" : req.body.email});
});

// Persistir Archivo JSON
function writeUserDataToFile(data) {
 var fs = require('fs');
 var jsonUserData = JSON.stringify(data);
 fs.writeFile("./users.json", jsonUserData, "utf8",
  function(err) { //función manejadora para gestionar errores de escritura
    if(err) {
      console.log(err);
    } else {
      console.log("Datos escritos en 'users.json'.");
    }
  })
}
